<?php
// could probably do this a little more smart but it fits the purpose :)
// also would have been nice to parse the data.txt file and dynamically add actions/discounts/product, it's fairly easily doable with the following function, have't implemented it

//parseDate(); // uncomment this line to automatically parse the data.txt

//parse data.txt
function parseDate(){
    try{
        $txt_file = file_get_contents('./data/data.txt');


        $rows        = explode("\n", $txt_file);

        $array = [];

        foreach ($rows as $row){
            if (strpos($row, '(') !== false && strpos($row, ')') !== false) { // is a header
                $data = explode(" (", $row);
                $file = strtolower($data[0]);
                $header = str_replace( ")", "", $data[1]);
                $header = explode( ", ", $header);
            }else if(isset($file) && isset($header) && $row!=''){
                $data = explode( " ", $row);
                $array[$file][] = array_combine($header, $data);
            }
        }


        foreach ($array as $file => $data){
            file_put_contents('./data/'.$file.'.json', json_encode($data));
        }


    }catch(Exception $e){

    }

}

// initiate actions
try {
    $actions = json_decode(file_get_contents('./data/actions.json'));
}catch(Exception $e){
    printf("%s".PHP_EOL, 'No actions available!');
}

//initiate products
$products = [];
try {
    $productsData = json_decode(file_get_contents('./data/products.json'));
    foreach ($productsData as $productData) {
        $products[$productData->id] = new Product($productData->id, $productData->name, $productData->price, $productData->available_quantity);
    }
}catch(Exception $e){
    printf("%s".PHP_EOL, 'No products available!');
}