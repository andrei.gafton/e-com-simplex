# e-com-simplex

Topic:
Shop application
 
Instructions:
Create 3 Clases
Product
CartItem
Cart
 
Analyze file data.txt and:
1. create the products
2. simulate user actions
3. apply discount accordingly
4. when running 'php index.php' it should print in the console:
  - list with each CartItem (product name, quantity, price, total price )
  - total number of products in the cart
  - discount
  - total price
 
Notes:
*) you can asume all data is correctly formatted, no validation is needed
*) ignore add actions that exceed the available quantity of the product
*) delete CartItem from Cart if after a remove action the quantity is 0 or less

