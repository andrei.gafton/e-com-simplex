<?php


class Cart
{

    private $discounts = [];
    public $cartItems = [];
    public $discount = 0;
    public $subtotal = 0;
    public $total = 0;
    public $totalItems = 0;

    public function __construct()
    {
        //probably can do this  better but fits the purpose :)
        $discounts = json_decode(file_get_contents('./data/discounts.json'));

        foreach ($discounts as $discount){
            $this->discounts[(int)$discount->min_price] = $discount->percent;
        }

        //make sure that the min price is ascending in order to get the max discount in Cart->calculateDiscount
        ksort($this->discounts);
    }

    /**
     * @return array
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     */
    public function setSubtotal($subtotal)
    {
        $this->subtotal = $subtotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return int
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @param int $totalItems
     */
    public function setTotalItems($totalItems)
    {
        $this->totalItems = $totalItems;
    }

    /**
     * @return array
     */
    public function getCartItems()
    {
        return $this->cartItems;
    }

    /**
     * add items based on product and quantity or increment items already in cart
     * @param Product $product
     * @param int $quantity
     * @return bool
     */
    public function addItem(Product $product, $quantity = 1){
        if(!$product->hasAvailability($quantity)){
            printf("Product %s is not available in selected quantity!".PHP_EOL, $product->getName());
            return false;
        }

        // if product is in cart then just increment the quantity else add a new CartItem
        if($this->productInCart($product)){
            $cartItem = $this->cartItems[$product->getId()];
            $cartItem->quantity += $quantity;
        }else{
            $this->cartItems[$product->getId()] = new CartItem($product, $quantity);
        }

        $this->updateCart();
        $product->decrementAvailableQuantity($quantity);
        return true;
    }

    /**
     * remove items from cart or decrement items already in cart
     * @param Product $product
     * @param int $quantity
     * @return bool
     */
    public function removeItem(Product $product, $quantity = 1){
        if($this->productInCart($product)){

            $cartItem = $this->cartItems[$product->getId()];
            $newQuantity = $cartItem->getQuantity() - $quantity;
            $cartItem->setQuantity($newQuantity);

            // if the new quantity is lower or equal to 0 then remove the CartItem from the cart
            if($newQuantity <= 0){
                unset($this->cartItems[$product->getId()]);
            }

            $this->updateCart();
            $product->incrementAvailableQuantity($quantity - $newQuantity);
            return true;
        }

        return false;
    }

    /**
     *  pretty print the cart information, could have used a beautifier
     */
    public function cartToString(){
        printf("%s".PHP_EOL, "------------------------");
        foreach ($this->getCartItems() as $cartItem){
            $cartItem->itemToString();
        }
        printf("Items | %d".PHP_EOL, $this->getTotalItems());
        printf("Sub-Total | %.2f".PHP_EOL, $this->getSubtotal());
        printf("Discount | %.2f%%".PHP_EOL, $this->getDiscount() * 100);
        printf("Total | %.2f".PHP_EOL, $this->getTotal());
        printf("%s".PHP_EOL, "------------------------");
    }

    /**
     * check if the Product exists in the CartItems present in the Cart
     * @param Product $product
     * @return bool
     */
    private function productInCart(Product $product){
        if( array_key_exists($product->getId(), $this->getCartItems()) ) return true;

        return false;
    }

    /**
     *might be redundant :) but maybe more actions on update for future
     */
    private function updateCart(){
        $this->calculateSubtotal()
            ->calculateDiscount()
            ->calculateTotal()
            ->calculateTotalItems();
    }

    /**
     * calculate the subtotal by summing up the total prices of each available cart item
     * @return $this
     */
    private function calculateSubtotal(){
        $subTotal = 0;
        foreach ($this->getCartItems() as $cartItem){
            $subTotal += $cartItem->getTotalPrice();
        }

        $this->setSubtotal($subTotal);
        return $this;
    }

    /**
     * calculate the discount applied based on the subtotal
     * @return $this
     */
    private function calculateDiscount(){
        $finalPercentage = 0;
        foreach ($this->getDiscounts() as $discountThreshold => $percentage) {
            if($this->getSubtotal() >= $discountThreshold){
                $finalPercentage = $percentage;
            }
        }

        $this->setDiscount($finalPercentage / 100);
        return $this;
    }

    /**
     * calculate the total by applying the discount to the subtotal
     * @return $this
     */
    private function calculateTotal(){
        $total = $this->getSubtotal() * (1-$this->getDiscount());

        $this->setTotal($total);
        return $this;
    }

    /**
     * calculate the total items by summing up the quantity of each available cart item
     * @return $this
     */
    private function calculateTotalItems(){
        $totalItems = 0;
        foreach ($this->getCartItems() as $cartItem){
            $totalItems += $cartItem->getQuantity();
        }

        $this->setTotalItems($totalItems);
        return $this;
    }
}