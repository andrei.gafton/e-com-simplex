<?php


class Product
{
    private $id;
    public $name;
    public $price;
    public $available_quantity;

    public function __construct($id, $name, $price, $available_quantity)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->available_quantity = $available_quantity;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getAvailableQuantity()
    {
        return $this->available_quantity;
    }

    /**
     * @param mixed $available_quantity
     */
    public function setAvailableQuantity($available_quantity)
    {
        $this->available_quantity = $available_quantity;
    }

    /**
     * check if the Product has enough available_quantity for requested quantity
     * @param $quantity
     * @return bool
     */
    public function hasAvailability($quantity){
        if($quantity > $this->getAvailableQuantity()){
            return false;
        }

        return true;
    }

    /**
     * increase the available_quantity after a removal from Cart
     * @param $value
     */
    public function incrementAvailableQuantity($value){
        $newQty = $this->getAvailableQuantity() + $value;
        $this->setAvailableQuantity($newQty);
    }

    /**
     * decrease the available_quantity after an addition to Cart
     * @param $value
     */
    public function decrementAvailableQuantity($value){
        $newQty = $this->getAvailableQuantity() - $value;
        $this->setAvailableQuantity($newQty);
    }
}