<?php


class CartItem
{
    private $id;
    public $name;
    public $price;
    public $quantity;

    public function __construct(Product $product, $quantity)
    {
        $this->id = $product->getId();
        $this->name = $product->getName();
        $this->price = $product->getPrice();
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * multiply the unit price by quantity
     * @return float
     */
    public function getTotalPrice(){
        return $this->getPrice() * $this->getQuantity();
    }

    /**
     *  pretty print the cart item information, could have used a beautifier
     */
    public function itemToString(){
        printf("%dx | %s | %.2f | %.2f".PHP_EOL, $this->getQuantity(), $this->getName(), $this->getPrice(), $this->getTotalPrice());
    }
}