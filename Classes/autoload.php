<?php
// custom exception error handler
function exceptions_error_handler($severity, $message, $filename, $lineno) {
    custom_log($severity, $message);
    throw new ErrorException($message, 0, $severity, $filename, $lineno);
}
set_error_handler('exceptions_error_handler');

//custom error logging
function custom_log($severity, $message){
    $log  = date("j.n.Y g:i a"). "|Error[". $severity . "]: ". $message .PHP_EOL;
    file_put_contents('./logs/log_'.date("j.n.Y").'.log', $log, FILE_APPEND);
}

//autoload classes
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});
