<?php
require './Classes/autoload.php';

session_start();

require './init_data.php';

printf("%s".PHP_EOL, "------------------------");
printf("%s".PHP_EOL, 'Hello World! Shop data has been initialised!');
printf("%s".PHP_EOL, "------------------------");

$cart = new Cart();

try{
    printf("%s".PHP_EOL, "Start Simulation!");
    printf("%s".PHP_EOL, "------------------------");
    foreach ($actions as $actionData){
        $action = $actionData->actions;
        $product = $products[$actionData->product_id];
        $quantity = $actionData->quantity;

        printf("%s %dx%s...".PHP_EOL, ucfirst($action), $quantity, $product->getName());

        $cart->{$action."Item"}($product, $quantity);
        $cart->cartToString();
    }
}catch(Exception $exception){
    printf("%s".PHP_EOL, 'An error occurred in simulating actions!');
}

session_destroy();